package edu.uprm.cse.datastructures.cardealer.util;

public interface ReverseIterator<E>{
	public boolean hasPrevious();
	public E previous();
}
