package edu.uprm.cse.datastructures.cardealer.util;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import edu.uprm.cse.datastructures.cardealer.model.*;
import edu.uprm.cse.datastructures.cardealer.ServerList;

@Path("/cars")
public class CarManager {

	private static CircularSortedDoublyLinkedList<Car> cList = ServerList.getList();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		/** @Message200: OK. */
		if(cList.isEmpty()) return new Car[0];
		Car[] result = new Car[cList.size()];
		int i = 0;
		while (i < result.length) { result[i] = cList.get(i); i++; }
		/** @Message200: OK. */
		return result;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		if(!cList.isEmpty()) {
			for (int i = 0; i < cList.size(); i++) {
				if (cList.get(i).getCarId() == id) {
					/** @Message200: OK. */
					return cList.get(i);
				}
			} 
		}
		/** @Error404: Car not found */
		throw new WebApplicationException(404);	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
//		for (int i = 0; i < cList.size(); i++) {
//			if (cList.get(i).getCarId() == car.getCarId()){
//				/** @Error400: Bad Request. */
//				return Response.status(Response.Status.BAD_REQUEST).build();
//			}
//		}

		cList.add(car);
		/** @Message201: OK. */
		return Response.status(201).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == car.getCarId()) {
				cList.remove(i);	cList.add(car);
				/** @Message200: OK. */
				return Response.status(Response.Status.OK).build();
			}
		}
		/** @Error404: Car not found */
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == id) {
				cList.remove(i);
				/** @Message200: OK. */
				return Response.status(Response.Status.OK).build();
			}
		}
		/** @Error404: Car not found */
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
