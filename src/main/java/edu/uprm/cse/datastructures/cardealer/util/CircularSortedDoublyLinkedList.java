package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{


	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node(){
			this.element = null;
			this.next = this.prev = null;
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> comparator;

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.currentSize = 0;
		this.header = new Node<>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.comparator = comparator;
	}

	private class LinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		public LinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}
		
		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} else throw new NoSuchElementException();
		}

	}
	
	private class LinkedListIndexIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		public LinkedListIndexIterator(int i) {
			this.nextNode = (Node<E>) header.getNext();
			int index = 0;
			while(index != i) {	nextNode = nextNode.getNext(); index++;	}
		}

		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}
		
		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} else throw new NoSuchElementException();
		}
	}
	
	private class LinkedListReverseIterator<E> implements ReverseIterator<E>{
		private Node<E> prevNode;

		public LinkedListReverseIterator() {
			this.prevNode = (Node<E>) header.getPrev();
		}

		public boolean hasPrevious() {
			return prevNode.getElement() != null;
		}
		
		public E previous() {
			if(this.hasPrevious()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			} else throw new NoSuchElementException();
		}

	}
	
	private class LinkedListReverseIndexIterator<E> implements ReverseIterator<E>{
		private Node<E> prevNode;

		public LinkedListReverseIndexIterator(int i) {
			this.prevNode = (Node<E>) header.getPrev();
			int index = currentSize-1;
			while(index != i) {	prevNode = prevNode.getPrev(); index--;	}
		}

		public boolean hasPrevious() {
			return prevNode.getElement() != null;
		}
		
		public E previous() {
			if(this.hasPrevious()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			} else throw new NoSuchElementException();
		}
	}
			
	public Iterator<E> iterator(int i) 	{	return new LinkedListIndexIterator<E>(i);					}

	@Override
	public Iterator<E> iterator() 		{	return new LinkedListIterator<E>();							}
	
	public ReverseIterator<E> reverseIterator() { return new LinkedListReverseIterator<E>();			}
	
	public ReverseIterator<E> reverseIterator(int i) { return new LinkedListReverseIndexIterator<E>(i);	}
	
	@Override
	public int size()					{	return this.currentSize;									}

	@Override
	public boolean contains(E e)		{	return this.firstIndex(e) >= 0;								}

	@Override
	public boolean isEmpty()			{	return this.currentSize == 0;								}

	@Override
	public E first()					{	return this.header.getNext().getElement();					}

	@Override
	public E last()						{	return this.header.getPrev().getElement();					}
	
	@Override
	public void clear()					{	while ( !this.isEmpty() )  this.remove(0); 					}

	@Override
	public int removeAll(E obj)		{
		int counter = 0;
		while(this.remove(obj)) counter++;
		return counter;
	}

	@Override
	public boolean remove(E obj)	{
		if(this.isEmpty()) return false;
		if(obj==null) return false;
		Node<E> newNode = this.header.getNext();

		int i = 0;
		while(i<this.currentSize) {
			if(this.comparator.compare(newNode.getElement(), obj)	==	0) {

				newNode.getPrev().setNext(newNode.getNext());
				newNode.getNext().setPrev(newNode.getPrev());
				newNode.setElement(null); newNode.setNext(null); newNode.setPrev(null);
				this.currentSize--;
				return true;

			} else {	newNode = newNode.getNext();	i++;	}
		}
		return false;

	}

	@Override
	public boolean remove(int index) {	
		if((index < 0) || (index >= this.currentSize)) 
			throw new IndexOutOfBoundsException("Wrong index.");
		
		Node<E> newNode = header.getNext();
		int i = 0;
		while(i != index) {	newNode = newNode.getNext(); i++;	}
		
		newNode.getPrev().setNext(newNode.getNext());
		newNode.getNext().setPrev(newNode.getPrev());
		newNode.setElement(null); newNode.setNext(null); newNode.setPrev(null);
		this.currentSize--;
		return true;
		}

	@Override
	public E get(int index)			{
		if((index < 0) || (index >= this.currentSize)) 
			throw new IndexOutOfBoundsException("Wrong index.");
		
		Node<E> result = this.header.getNext();
		int i = 0;
		while(i != index) {	result = result.getNext(); i++;	}
		return result.getElement();
	}

	@Override
	public int firstIndex(E e)		{
		Node<E> temp = this.header.getNext();
		int i = 0;
		while(i<this.currentSize) {
			if(this.comparator.compare(temp.getElement(), e) != 0) {
				temp = temp.getNext(); i++;
			} else return i;
		} return -1;
	}

	@Override
	public int lastIndex(E e)		{
		Node<E> temp = this.header.getPrev();
		int i = this.currentSize - 1;
		while(i > 0) {
			if(this.comparator.compare(temp.getElement(), e) != 0) {
				temp = temp.getPrev(); i--;
			} else return i;
		} return -1;
	}

	@Override
	public boolean add(E obj)		{	
		if(obj==null) return false;
		Node<E> newNode = new Node<E>();
		newNode.setElement(obj);
		
		if(this.isEmpty()) {
			this.header.setNext(newNode); this.header.setPrev(newNode);
			newNode.setNext(this.header); newNode.setPrev(this.header);
			this.currentSize++; return true;
		}
		
		Node<E> temp = this.header.getNext();
		boolean running = true;
		while(running) {
			
			if(this.comparator.compare(temp.getElement(), newNode.getElement()) == 0) {

				newNode.setPrev(temp.getPrev());  newNode.setNext(temp);
				temp.getPrev().setNext(newNode);  temp.setPrev(newNode);
				this.currentSize++;  return true;
				
			} else if ((this.comparator.compare(temp.getElement(), newNode.getElement()) < 0) && (temp.getNext().getElement()==null)) {

				newNode.setPrev(temp);  newNode.setNext(this.header);
				this.header.setPrev(newNode);  temp.setNext(newNode);
				this.currentSize++;  return true;
				
			} else if ((this.comparator.compare(temp.getElement(), newNode.getElement()) > 0) && (temp.getPrev().getElement()==null)) {

				newNode.setNext(temp);  newNode.setPrev(this.header);
				this.header.setNext(newNode);  temp.setPrev(newNode);
				this.currentSize++;  return true;
				
			} else if ((this.comparator.compare(temp.getElement(), newNode.getElement()) < 0) 
					&& (this.comparator.compare(temp.getNext().getElement(), newNode.getElement()) > 0)) {

				newNode.setPrev(temp);  newNode.setNext(temp.getNext());
				temp.getNext().setPrev(newNode); temp.setNext(newNode);
				this.currentSize++; return true;
				
			} else if (temp.getNext().getElement()==null) {

				newNode.setPrev(temp);  newNode.setNext(this.header);
				temp.setNext(newNode);  this.header.setPrev(newNode);
				this.currentSize++; return true;
				
			}
			
			temp = temp.getNext();
			
		}	return false;
		
	}
	
}
