package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class ServerList {

	/**
	 * 
	 * @servesTo instance a list inside the server.
	 * 
	 */

	private static CircularSortedDoublyLinkedList<Car> 
	cList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	public static CircularSortedDoublyLinkedList<Car> getList()	 	{	return cList;	}
	public static void resetCars()									{	cList.clear();	}

}